package ee.ut.bpmn.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

public class MXMLgenerator2 {
	
	private Date curdate;
	
	private final int MIN = 60000;
	Map<Integer, String> activities;
	Set<Integer> sinks;
	Multimap<Integer, Integer> relations;
	Map<String, Double> freq;

//-------------------------------------------------------------------------------------------------------------
	public static void main(String[] args) {
		MXMLgenerator2 gen = new MXMLgenerator2();

		gen.createGeneratedBase();
		gen.createMXML(1000, "base");
	}	
//-------------------------------------------------------------------------------------------------------------	
	
	public MXMLgenerator2() {
		curdate = new Date(System.currentTimeMillis());
		clearAll();
	}
	
	private void clearAll() {
		activities = new HashMap<Integer, String>();
		sinks = new HashSet<Integer>();
		relations = HashMultimap.create();
		freq = new HashMap<String, Double>();
	}
	
	private String getTimeStamp(int minutes) {		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		curdate = new Date(curdate.getTime() + minutes * MIN + Math.round(new Random().nextDouble() * 1000));
		
		String datestr = dateFormat.format(curdate);
		
		datestr = datestr.replace(" ", "T") + ".000+00:00";
		
		return datestr;
	}
	
	private String getActivity(String actname) {
		String activity = "<AuditTrailEntry>\n";
		activity += "<WorkflowModelElement>" + actname + "</WorkflowModelElement>\n";
		activity += "<EventType>complete</EventType>\n";
		activity += "<Timestamp>" +  getTimeStamp(5) + "</Timestamp>\n";
		activity += "<Originator>Default Resource</Originator>\n";
		activity += "</AuditTrailEntry>\n";

		return activity;
	}
	
	private String getData() {
		return "<Data>\n<Attribute name=\"LogType\">MXML.EnactmentLog</Attribute>\n</Data>\n";
	}
	
	private String getTrace(List<Integer> curInstance, int id) {
		String trace = "<ProcessInstance id=\"" + id + "\">\n";
		
		trace += getData();
		
		for (int i = 0; i < curInstance.size(); i++) {
			trace += getActivity(activities.get(curInstance.get(i)));
		}
		
		trace += "</ProcessInstance>\n";
		
		return trace;
	}
	
	private String getHeader(String path, String filename) {
		String headerfile = "";

		try {
			headerfile = new String(Files.readAllBytes(Paths.get(path + filename)));
		}
		catch (IOException e) {
			System.out.println(e.getStackTrace());
		}
		
		return headerfile;
	}
	
	private static String getFooter() {
		return "</Process></WorkflowLog>";
	}
	
	public void createMXML(int tracecount, String modelname) {
		String path = "eventstr-confchecking/models/RunningExample/";
		String headerfile = "header.txt";
		String mxmlfile = ".mxml";
		
		String mxml = getHeader(path, headerfile);
		
		for (int i = 1; i <= tracecount; i++) {
			mxml += getTrace(getInstance(), i);
		}

		mxml += getFooter();
		
		mxmlfile = modelname + mxmlfile;

		try {
			File newTextFile = new File(path + mxmlfile);
            FileWriter fileWriter = new FileWriter(newTextFile);
            fileWriter.write(mxml);
            fileWriter.close();
		}
		catch(Exception e) {
			System.out.println(e.getStackTrace());
		}
	}
	
	public List<Integer> getInstance() {
		List<Integer> instance = new ArrayList<Integer>();
		
		int curact, newact;
		String currentRel;
		Double prob, curprob;
		Set<Integer> nextact;
		
		// create starting point
		curact = newact = 1;
		instance.add(1);
		
		nextact = new HashSet<Integer>(relations.get(curact));

		while (nextact.size() > 0) {
			curprob = 1.0;
			prob = Math.random();
			for (int a: nextact) {
				currentRel = "[" + curact + ", " + a + "]";
				if ((prob < freq.get(currentRel)) && (freq.get(currentRel) <= curprob)) {
					newact = a; 
					curprob = freq.get(currentRel);
				}
			}
			instance.add(newact);
			curact = newact;
			nextact = new HashSet<Integer>(relations.get(curact));
		}
		return instance;
	}
	
	public void createGeneratedBase() {
		clearAll();
		
		activities.put(1, "Check application form completeness");
		activities.put(2, "Return application to applicant");
		activities.put(3, "Receive updated application");
		activities.put(4, "Check revised form");
		activities.put(5, "Check credit history");
		activities.put(15, "Check credit history");
		activities.put(25, "Check credit history");
		activities.put(6, "Assess loan risk");
		activities.put(16, "Assess loan risk");
		activities.put(26, "Assess loan risk");
		activities.put(7, "Appraise property");
		activities.put(17, "Appraise property");
		activities.put(27, "Appraise property");
		activities.put(8, "Assess eligibility");
		activities.put(9, "Reject application");
		activities.put(10, "Prepare acceptance pack");
		activities.put(11, "Check if home insurance quote is requested");
		activities.put(12, "Send home insurance quote");
		activities.put(13, "Verify repayment agreement");
		activities.put(14, "Cancel application");
		activities.put(18, "Approve application");
		
		relations.put(1, 5); //0.5
		freq.put("[1, 5]", 0.1667);
		relations.put(1, 15); //0.5
		freq.put("[1, 15]", 0.3333);
		relations.put(1, 27); //0.5
		freq.put("[1, 27]", 0.5);
		relations.put(1, 2); //0.5
		freq.put("[1, 2]", 1.0);
		relations.put(2, 3); //1.0
		freq.put("[2, 3]", 1.0);
		relations.put(3, 4); //1.0
		freq.put("[3, 4]", 1.0);
		relations.put(4, 5); //0.1667
		freq.put("[4, 5]", 0.1667);
		relations.put(4, 15); //0.1667
		freq.put("[4, 15]", 0.3333);
		relations.put(4, 27); //0.1667
		freq.put("[4, 27]", 0.5);
		relations.put(4, 2); //0.5
		freq.put("[4, 2]", 1.0);
		
		// interleaving concurrency
		relations.put(5, 6); //1.0
		freq.put("[5, 6]", 1.0);
		relations.put(6, 7); //1.0
		freq.put("[6, 7]", 1.0);
		relations.put(15, 17); //1.0
		freq.put("[15, 17]", 1.0);
		relations.put(17, 16); //1.0
		freq.put("[17, 16]", 1.0);
		relations.put(27, 25); //1.0
		freq.put("[27, 25]", 1.0);
		relations.put(25, 26); //1.0
		freq.put("[25, 26]", 1.0);
		relations.put(16, 8); //1.0
		freq.put("[16, 8]", 1.0);
		relations.put(26, 8); //1.0
		freq.put("[26, 8]", 1.0);
		relations.put(7, 8); //1.0
		freq.put("[7, 8]", 1.0);
		
		// continuing regular flow of the process
		relations.put(8, 9); //0.5
		freq.put("[8, 9]", 0.5);
		relations.put(8, 10); //0.5
		freq.put("[8, 10]", 1.0);
		relations.put(10, 11); //1.0
		freq.put("[10, 11]", 1.0);
		relations.put(11, 12); //0.5
		freq.put("[11, 12]", 0.5);
		relations.put(11, 13); //0.5
		freq.put("[11, 13]", 1.0);
		relations.put(12, 13); //1.0
		freq.put("[12, 13]", 1.0);
		relations.put(13, 14); //0.5
		freq.put("[13, 14]", 0.5);
		relations.put(13, 18); //0.5
		freq.put("[13, 18]", 1.0);
	} //base
	
	public void createGeneratedI1() {
		clearAll();
		
		activities.put(1, "Check application form completeness");
		activities.put(2, "Return application to applicant");
		activities.put(3, "Receive updated application");
		activities.put(4, "Check revised form");
		activities.put(5, "Check credit history");
		activities.put(15, "Check credit history");
		activities.put(25, "Check credit history");
		activities.put(6, "Assess loan risk");
		activities.put(16, "Assess loan risk");
		activities.put(26, "Assess loan risk");
		activities.put(7, "Appraise property");
		activities.put(17, "Appraise property");
		activities.put(27, "Appraise property");
//		activities.put(8, "Assess eligibility");
		activities.put(9, "Reject application");
		activities.put(10, "Prepare acceptance pack");
		activities.put(11, "Check if home insurance quote is requested");
		activities.put(12, "Send home insurance quote");
		activities.put(13, "Verify repayment agreement");
		activities.put(14, "Cancel application");
		activities.put(18, "Approve application");
		
		relations.put(1, 5); //0.5
		freq.put("[1, 5]", 0.1667);
		relations.put(1, 15); //0.5
		freq.put("[1, 15]", 0.3333);
		relations.put(1, 27); //0.5
		freq.put("[1, 27]", 0.5);
		relations.put(1, 2); //0.5
		freq.put("[1, 2]", 1.0);
		relations.put(2, 3); //1.0
		freq.put("[2, 3]", 1.0);
		relations.put(3, 4); //1.0
		freq.put("[3, 4]", 1.0);
		relations.put(4, 5); //0.1667
		freq.put("[4, 5]", 0.1667);
		relations.put(4, 15); //0.1667
		freq.put("[4, 15]", 0.3333);
		relations.put(4, 27); //0.1667
		freq.put("[4, 27]", 0.5);
		relations.put(4, 2); //0.5
		freq.put("[4, 2]", 1.0);
		
		// interleaving concurrency
		relations.put(5, 6); //1.0
		freq.put("[5, 6]", 1.0);
		relations.put(6, 7); //1.0
		freq.put("[6, 7]", 1.0);
		relations.put(15, 17); //1.0
		freq.put("[15, 17]", 1.0);
		relations.put(17, 16); //1.0
		freq.put("[17, 16]", 1.0);
		relations.put(27, 25); //1.0
		freq.put("[27, 25]", 1.0);
		relations.put(25, 26); //1.0
		freq.put("[25, 26]", 1.0);
		
		relations.put(16, 9); //0.5
		freq.put("[16, 9]", 0.5);
		relations.put(26, 9); //0.5
		freq.put("[26, 9]", 0.5);
		relations.put(7, 9); //0.5
		freq.put("[7, 9]", 0.5);
		relations.put(16, 10); //0.5
		freq.put("[16, 10]", 1.0);
		relations.put(26, 10); //0.5
		freq.put("[26, 10]", 1.0);
		relations.put(7, 10); //0.5
		freq.put("[7, 10]", 1.0);
		
		// continuing regular flow of the process
		relations.put(10, 11); //1.0
		freq.put("[10, 11]", 1.0);
		relations.put(11, 12); //0.5
		freq.put("[11, 12]", 0.5);
		relations.put(11, 13); //0.5
		freq.put("[11, 13]", 1.0);
		relations.put(12, 13); //1.0
		freq.put("[12, 13]", 1.0);
		relations.put(13, 14); //0.5
		freq.put("[13, 14]", 0.5);
		relations.put(13, 18); //0.5
		freq.put("[13, 18]", 1.0);
	} //I1
	
	public void createGeneratedI2() {
		clearAll();
		
		activities.put(1, "Check application form completeness");
		activities.put(2, "Return application to applicant");
		activities.put(3, "Receive updated application");
		activities.put(4, "Check revised form");
		activities.put(5, "Check credit history");
		activities.put(15, "Check credit history");
		activities.put(25, "Check credit history");
		activities.put(6, "Assess loan risk");
		activities.put(16, "Assess loan risk");
		activities.put(26, "Assess loan risk");
		activities.put(36, "Assess loan risk");
		activities.put(7, "Appraise property");
		activities.put(17, "Appraise property");
		activities.put(27, "Appraise property");
		activities.put(8, "Assess eligibility");
		activities.put(9, "Reject application");
		activities.put(10, "Prepare acceptance pack");
		activities.put(11, "Check if home insurance quote is requested");
		activities.put(12, "Send home insurance quote");
		activities.put(13, "Verify repayment agreement");
		activities.put(14, "Cancel application");
		activities.put(18, "Approve application");
		
		relations.put(1, 5); //0.5
		freq.put("[1, 5]", 0.1667);
		relations.put(1, 15); //0.5
		freq.put("[1, 15]", 0.3333);
		relations.put(1, 27); //0.5
		freq.put("[1, 27]", 0.5);
		relations.put(1, 2); //0.5
		freq.put("[1, 2]", 1.0);
		relations.put(2, 3); //1.0
		freq.put("[2, 3]", 1.0);
		relations.put(3, 4); //1.0
		freq.put("[3, 4]", 1.0);
		relations.put(4, 5); //0.1667
		freq.put("[4, 5]", 0.1667);
		relations.put(4, 15); //0.1667
		freq.put("[4, 15]", 0.3333);
		relations.put(4, 27); //0.1667
		freq.put("[4, 27]", 0.5);
		relations.put(4, 2); //0.5
		freq.put("[4, 2]", 1.0);
		
		// interleaving concurrency
		relations.put(5, 6); //1.0
		freq.put("[5, 6]", 1.0);
		relations.put(6, 7); //1.0
		freq.put("[6, 7]", 1.0);
		relations.put(15, 17); //1.0
		freq.put("[15, 17]", 1.0);
		relations.put(17, 16); //1.0
		freq.put("[17, 16]", 1.0);
		relations.put(27, 25); //1.0
		freq.put("[27, 25]", 1.0);
		relations.put(25, 26); //1.0
		freq.put("[25, 26]", 1.0);
		relations.put(16, 8); //1.0
		freq.put("[16, 8]", 1.0);
		relations.put(26, 8); //1.0
		freq.put("[26, 8]", 1.0);
		relations.put(7, 8); //1.0
		freq.put("[7, 8]", 1.0);
		
		// continuing regular flow of the process
		relations.put(8, 9); //0.5
		freq.put("[8, 9]", 0.5);
		relations.put(8, 10); //0.5
		freq.put("[8, 10]", 1.0);
		relations.put(10, 11); //1.0
		freq.put("[10, 11]", 1.0);
		relations.put(11, 12); //0.5
		freq.put("[11, 12]", 0.5);
		relations.put(11, 13); //0.5
		freq.put("[11, 13]", 1.0);
		relations.put(12, 13); //1.0
		freq.put("[12, 13]", 1.0);
		relations.put(13, 36); //1.0
		freq.put("[13, 36]", 1.0);
		relations.put(36, 14); //0.5
		freq.put("[36, 14]", 0.5);
		relations.put(36, 18); //0.5
		freq.put("[36, 18]", 1.0);
	} //I2
	
	public void createGeneratedI3() {
		clearAll();
		
		activities.put(1, "Check application form completeness");
		activities.put(2, "Return application to applicant");
		activities.put(3, "Receive updated application");
		activities.put(4, "Check revised form");
		activities.put(5, "Check credit history");
		activities.put(15, "Check credit history");
		activities.put(25, "Check credit history");
		activities.put(6, "Assess loan risk");
		activities.put(16, "Assess loan risk");
		activities.put(26, "Assess loan risk");
		activities.put(7, "Appraise property");
		activities.put(17, "Appraise property");
		activities.put(27, "Appraise property");
		activities.put(8, "Assess eligibility");
		activities.put(9, "Reject application");
		activities.put(10, "Prepare acceptance pack");
		activities.put(11, "Check if home insurance quote is requested");
		activities.put(12, "Send home insurance quote");
		activities.put(13, "Replaced activity");
		activities.put(14, "Cancel application");
		activities.put(18, "Approve application");
		
		relations.put(1, 5); //0.5
		freq.put("[1, 5]", 0.1667);
		relations.put(1, 15); //0.5
		freq.put("[1, 15]", 0.3333);
		relations.put(1, 27); //0.5
		freq.put("[1, 27]", 0.5);
		relations.put(1, 2); //0.5
		freq.put("[1, 2]", 1.0);
		relations.put(2, 3); //1.0
		freq.put("[2, 3]", 1.0);
		relations.put(3, 4); //1.0
		freq.put("[3, 4]", 1.0);
		relations.put(4, 5); //0.1667
		freq.put("[4, 5]", 0.1667);
		relations.put(4, 15); //0.1667
		freq.put("[4, 15]", 0.3333);
		relations.put(4, 27); //0.1667
		freq.put("[4, 27]", 0.5);
		relations.put(4, 2); //0.5
		freq.put("[4, 2]", 1.0);
		
		// interleaving concurrency
		relations.put(5, 6); //1.0
		freq.put("[5, 6]", 1.0);
		relations.put(6, 7); //1.0
		freq.put("[6, 7]", 1.0);
		relations.put(15, 17); //1.0
		freq.put("[15, 17]", 1.0);
		relations.put(17, 16); //1.0
		freq.put("[17, 16]", 1.0);
		relations.put(27, 25); //1.0
		freq.put("[27, 25]", 1.0);
		relations.put(25, 26); //1.0
		freq.put("[25, 26]", 1.0);
		relations.put(16, 8); //1.0
		freq.put("[16, 8]", 1.0);
		relations.put(26, 8); //1.0
		freq.put("[26, 8]", 1.0);
		relations.put(7, 8); //1.0
		freq.put("[7, 8]", 1.0);
		
		// continuing regular flow of the process
		relations.put(8, 9); //0.5
		freq.put("[8, 9]", 0.5);
		relations.put(8, 10); //0.5
		freq.put("[8, 10]", 1.0);
		relations.put(10, 11); //1.0
		freq.put("[10, 11]", 1.0);
		relations.put(11, 12); //0.5
		freq.put("[11, 12]", 0.5);
		relations.put(11, 13); //0.5
		freq.put("[11, 13]", 1.0);
		relations.put(12, 13); //1.0
		freq.put("[12, 13]", 1.0);
		relations.put(13, 14); //0.5
		freq.put("[13, 14]", 0.5);
		relations.put(13, 18); //0.5
		freq.put("[13, 18]", 1.0);
	} //I3	
	
	public void createGeneratedR1() {
		clearAll();
		
		activities.put(1, "Check application form completeness");
		activities.put(2, "Return application to applicant");
		activities.put(3, "Receive updated application");
		activities.put(4, "Check revised form");
		activities.put(5, "Check credit history");
		activities.put(15, "Check credit history");
		activities.put(25, "Check credit history");
		activities.put(6, "Assess loan risk");
		activities.put(16, "Assess loan risk");
		activities.put(26, "Assess loan risk");
		activities.put(7, "Appraise property");
		activities.put(17, "Appraise property");
		activities.put(27, "Appraise property");
		activities.put(8, "Assess eligibility");
		activities.put(9, "Reject application");
		activities.put(10, "Prepare acceptance pack");
		activities.put(11, "Check if home insurance quote is requested");
		activities.put(12, "Send home insurance quote");
		activities.put(13, "Verify repayment agreement");
		activities.put(14, "Cancel application");
		activities.put(18, "Approve application");
		
		relations.put(1, 5); //0.5
		freq.put("[1, 5]", 0.1667);
		relations.put(1, 15); //0.5
		freq.put("[1, 15]", 0.3333);
		relations.put(1, 27); //0.5
		freq.put("[1, 27]", 0.5);
		relations.put(1, 2); //0.5
		freq.put("[1, 2]", 1.0);
		relations.put(2, 3); //1.0
		freq.put("[2, 3]", 1.0);
		relations.put(3, 4); //1.0
		freq.put("[3, 4]", 1.0);
		relations.put(4, 5); //0.1667
		freq.put("[4, 5]", 0.1667);
		relations.put(4, 15); //0.1667
		freq.put("[4, 15]", 0.3333);
		relations.put(4, 27); //0.1667
		freq.put("[4, 27]", 0.5);
		relations.put(4, 2); //0.5
		freq.put("[4, 2]", 1.0);
		
		// interleaving concurrency
		relations.put(5, 6); //1.0
		freq.put("[5, 6]", 1.0);
		relations.put(6, 7); //1.0
		freq.put("[6, 7]", 1.0);
		relations.put(15, 17); //1.0
		freq.put("[15, 17]", 1.0);
		relations.put(17, 16); //1.0
		freq.put("[17, 16]", 1.0);
		relations.put(27, 25); //1.0
		freq.put("[27, 25]", 1.0);
		relations.put(25, 26); //1.0
		freq.put("[25, 26]", 1.0);
		relations.put(16, 8); //0.5 / 3
		freq.put("[16, 8]", 0.1667);
		relations.put(26, 8); //0.5 / 3
		freq.put("[26, 8]", 0.3333);
		relations.put(7, 8); //0.5 / 3
		freq.put("[7, 8]", 0.5);
		
		relations.put(16, 5); //0.5 / 9
		freq.put("[16, 5]", 0.5556);
		relations.put(26, 5); //0.5 / 9
		freq.put("[26, 5]", 0.6111);
		relations.put(7, 5); //0.5 / 9
		freq.put("[7, 5]", 0.6667);
		
		relations.put(16, 15); //0.5 / 9
		freq.put("[16, 15]", 0.7222);
		relations.put(26, 15); //0.5 / 9
		freq.put("[26, 15]", 0.7778);
		relations.put(7, 15); //0.5 / 9
		freq.put("[7, 15]", 0.8333);
		
		relations.put(16, 27); //0.5 / 9
		freq.put("[16, 27]", 0.8889);
		relations.put(26, 27); //0.5 / 9
		freq.put("[26, 27]", 0.9444);
		relations.put(7, 27); //0.5 / 9
		freq.put("[7, 27]", 1.0);
		
		// continuing regular flow of the process
		relations.put(8, 9); //0.5
		freq.put("[8, 9]", 0.5);
		relations.put(8, 10); //0.5
		freq.put("[8, 10]", 1.0);
		relations.put(10, 11); //1.0
		freq.put("[10, 11]", 1.0);
		relations.put(11, 12); //0.5
		freq.put("[11, 12]", 0.5);
		relations.put(11, 13); //0.5
		freq.put("[11, 13]", 1.0);
		relations.put(12, 13); //1.0
		freq.put("[12, 13]", 1.0);
		relations.put(13, 14); //0.5
		freq.put("[13, 14]", 0.5);
		relations.put(13, 18); //0.5
		freq.put("[13, 18]", 1.0);
	} //R1
	
	public void createGeneratedR2() {
		clearAll();
		
		activities.put(1, "Check application form completeness");
		activities.put(2, "Return application to applicant");
		activities.put(3, "Receive updated application");
		activities.put(4, "Check revised form");
		activities.put(5, "Check credit history");
		activities.put(15, "Check credit history");
		activities.put(25, "Check credit history");
		activities.put(6, "Assess loan risk");
		activities.put(16, "Assess loan risk");
		activities.put(26, "Assess loan risk");
		activities.put(7, "Appraise property");
		activities.put(17, "Appraise property");
		activities.put(27, "Appraise property");
		activities.put(8, "Assess eligibility");
		activities.put(9, "Reject application");
		activities.put(10, "Prepare acceptance pack");
		activities.put(11, "Check if home insurance quote is requested");
		activities.put(12, "Send home insurance quote");
		activities.put(13, "Verify repayment agreement");
		activities.put(14, "Cancel application");
		activities.put(18, "Approve application");
		
		relations.put(1, 5); //0.5
		freq.put("[1, 5]", 0.1667);
		relations.put(1, 15); //0.5
		freq.put("[1, 15]", 0.3333);
		relations.put(1, 27); //0.5
		freq.put("[1, 27]", 0.5);
		relations.put(1, 2); //0.5
		freq.put("[1, 2]", 1.0);
		relations.put(2, 3); //1.0
		freq.put("[2, 3]", 1.0);
		relations.put(3, 4); //1.0
		freq.put("[3, 4]", 1.0);
		relations.put(4, 5); //0.1667
		freq.put("[4, 5]", 0.1667);
		relations.put(4, 15); //0.1667
		freq.put("[4, 15]", 0.3333);
		relations.put(4, 27); //0.1667
		freq.put("[4, 27]", 0.5);
		relations.put(4, 2); //0.5
		freq.put("[4, 2]", 1.0);
		
		// interleaving concurrency
		relations.put(5, 6); //1.0
		freq.put("[5, 6]", 1.0);
		relations.put(6, 7); //1.0
		freq.put("[6, 7]", 1.0);
		relations.put(15, 17); //1.0
		freq.put("[15, 17]", 1.0);
		relations.put(17, 16); //1.0
		freq.put("[17, 16]", 1.0);
		relations.put(27, 25); //1.0
		freq.put("[27, 25]", 1.0);
		relations.put(25, 26); //1.0
		freq.put("[25, 26]", 1.0);
		relations.put(16, 8); //1.0
		freq.put("[16, 8]", 1.0);
		relations.put(26, 8); //1.0
		freq.put("[26, 8]", 1.0);
		relations.put(7, 8); //1.0
		freq.put("[7, 8]", 1.0);
		
		// continuing regular flow of the process
		relations.put(8, 9); //0.5
		freq.put("[8, 9]", 0.5);
		relations.put(8, 10); //0.25
		freq.put("[8, 10]", 0.75);
		relations.put(8, 12); //0.125
		freq.put("[8, 12]", 0.875);
		relations.put(8, 13); //0.125
		freq.put("[8, 13]", 1.0);
		relations.put(10, 11); //1.0
		freq.put("[10, 11]", 1.0);
		relations.put(11, 12); //0.5
		freq.put("[11, 12]", 0.5);
		relations.put(11, 13); //0.5
		freq.put("[11, 13]", 1.0);
		
		relations.put(12, 13); //1.0
		freq.put("[12, 13]", 1.0);
		relations.put(13, 14); //0.5
		freq.put("[13, 14]", 0.5);
		relations.put(13, 18); //0.5
		freq.put("[13, 18]", 1.0);
	} //R2
	
	public void createGeneratedR3() {
		clearAll();
		
		activities.put(1, "Check application form completeness");
		activities.put(2, "Return application to applicant");
		activities.put(3, "Receive updated application");
		activities.put(4, "Check revised form");
		activities.put(5, "Check credit history");
		activities.put(15, "Check credit history");
		activities.put(25, "Check credit history");
		activities.put(6, "Assess loan risk");
		activities.put(16, "Assess loan risk");
		activities.put(26, "Assess loan risk");
		activities.put(7, "Appraise property");
		activities.put(17, "Appraise property");
		activities.put(27, "Appraise property");
		activities.put(8, "Assess eligibility");
		activities.put(9, "Reject application");
		activities.put(10, "Prepare acceptance pack");
		activities.put(11, "Check if home insurance quote is requested");
		activities.put(12, "Send home insurance quote");
		activities.put(13, "Verify repayment agreement");
		activities.put(14, "Cancel application");
		activities.put(18, "Approve application");
		
		relations.put(1, 5); //0.5
		freq.put("[1, 5]", 0.1667);
		relations.put(1, 15); //0.5
		freq.put("[1, 15]", 0.3333);
		relations.put(1, 27); //0.5
		freq.put("[1, 27]", 0.5);
		relations.put(1, 2); //0.5
		freq.put("[1, 2]", 1.0);
		relations.put(2, 3); //1.0
		freq.put("[2, 3]", 1.0);
		relations.put(3, 4); //1.0
		freq.put("[3, 4]", 1.0);
		relations.put(4, 5); //0.1667
		freq.put("[4, 5]", 0.1667);
		relations.put(4, 15); //0.1667
		freq.put("[4, 15]", 0.3333);
		relations.put(4, 27); //0.1667
		freq.put("[4, 27]", 0.5);
		relations.put(4, 2); //0.5
		freq.put("[4, 2]", 1.0);
		
		// interleaving concurrency
		relations.put(5, 6); //1.0
		freq.put("[5, 6]", 1.0);
		relations.put(6, 7); //1.0
		freq.put("[6, 7]", 1.0);
		relations.put(15, 17); //1.0
		freq.put("[15, 17]", 1.0);
		relations.put(17, 16); //1.0
		freq.put("[17, 16]", 1.0);
		relations.put(27, 25); //1.0
		freq.put("[27, 25]", 1.0);
		relations.put(25, 26); //1.0
		freq.put("[25, 26]", 1.0);
		relations.put(16, 8); //1.0
		freq.put("[16, 8]", 1.0);
		relations.put(26, 8); //1.0
		freq.put("[26, 8]", 1.0);
		relations.put(7, 8); //1.0
		freq.put("[7, 8]", 1.0);
		
		// continuing regular flow of the process
		relations.put(8, 9); //0.5
		freq.put("[8, 9]", 0.5);
		relations.put(8, 10); //0.5
		freq.put("[8, 10]", 1.0);
		relations.put(10, 11); //1.0
		freq.put("[10, 11]", 1.0);
		relations.put(11, 12); //0.25
		freq.put("[11, 12]", 0.25);
		relations.put(11, 13); //0.75
		freq.put("[11, 13]", 1.0);
		relations.put(12, 13); //1.0
		freq.put("[12, 13]", 1.0);
		relations.put(13, 14); //0.5
		freq.put("[13, 14]", 0.5);
		relations.put(13, 18); //0.5
		freq.put("[13, 18]", 1.0);
	} //R3
	
	public void createGeneratedO1() {
		clearAll();
		
		activities.put(1, "Check application form completeness");
		activities.put(2, "Return application to applicant");
		activities.put(3, "Receive updated application");
		activities.put(4, "Check revised form");
		activities.put(5, "Check credit history");
		activities.put(15, "Check credit history");
		activities.put(25, "Check credit history");
		activities.put(6, "Assess loan risk");
		activities.put(16, "Assess loan risk");
		activities.put(26, "Assess loan risk");
		activities.put(7, "Appraise property");
		activities.put(17, "Appraise property");
		activities.put(27, "Appraise property");
		activities.put(8, "Assess eligibility");
		activities.put(9, "Reject application");
		activities.put(10, "Prepare acceptance pack");
		activities.put(11, "Check if home insurance quote is requested");
		activities.put(12, "Send home insurance quote");
		activities.put(13, "Verify repayment agreement");
		activities.put(14, "Cancel application");
		activities.put(18, "Approve application");
		
		relations.put(1, 5); //0.5
		freq.put("[1, 5]", 0.5);
		relations.put(1, 2); //0.5
		freq.put("[1, 2]", 1.0);
		relations.put(2, 3); //1.0
		freq.put("[2, 3]", 1.0);
		relations.put(3, 4); //1.0
		freq.put("[3, 4]", 1.0);
		relations.put(4, 5); //0.5
		freq.put("[4, 5]", 0.5);
		relations.put(4, 2); //0.5
		freq.put("[4, 2]", 1.0);
		
		// interleaving concurrency
		relations.put(5, 6); //1.0
		freq.put("[5, 6]", 1.0);
		relations.put(6, 7); //1.0
		freq.put("[6, 7]", 1.0);
		relations.put(15, 17); //1.0
		relations.put(7, 8); //1.0
		freq.put("[7, 8]", 1.0);
		
		// continuing regular flow of the process
		relations.put(8, 9); //0.5
		freq.put("[8, 9]", 0.5);
		relations.put(8, 10); //0.5
		freq.put("[8, 10]", 1.0);
		relations.put(10, 11); //1.0
		freq.put("[10, 11]", 1.0);
		relations.put(11, 12); //0.5
		freq.put("[11, 12]", 0.5);
		relations.put(11, 13); //0.5
		freq.put("[11, 13]", 1.0);
		relations.put(12, 13); //1.0
		freq.put("[12, 13]", 1.0);
		relations.put(13, 14); //0.5
		freq.put("[13, 14]", 0.5);
		relations.put(13, 18); //0.5
		freq.put("[13, 18]", 1.0);
	} //O1
	
	public void createGeneratedO2() {
		clearAll();
		
		activities.put(1, "Check application form completeness");
		activities.put(2, "Return application to applicant");
		activities.put(3, "Receive updated application");
		activities.put(4, "Check revised form");
		activities.put(5, "Check credit history");
		activities.put(15, "Check credit history");
		activities.put(6, "Assess loan risk");
		activities.put(16, "Assess loan risk");
		activities.put(7, "Appraise property");
		activities.put(17, "Appraise property");
		activities.put(27, "Appraise property");
		activities.put(8, "Assess eligibility");
		activities.put(9, "Reject application");
		activities.put(10, "Prepare acceptance pack");
		activities.put(11, "Check if home insurance quote is requested");
		activities.put(12, "Send home insurance quote");
		activities.put(13, "Verify repayment agreement");
		activities.put(14, "Cancel application");
		activities.put(18, "Approve application");
		
		relations.put(1, 5); //0.5
		freq.put("[1, 5]", 0.125);
		relations.put(1, 17); //0.5
		freq.put("[1, 17]", 0.25);
		relations.put(1, 6); //0.5
		freq.put("[1, 6]", 0.375);
		relations.put(1, 27); //0.5
		freq.put("[1, 27]", 0.5);
		relations.put(1, 2); //0.5
		freq.put("[1, 2]", 1.0);
		relations.put(2, 3); //1.0
		freq.put("[2, 3]", 1.0);
		relations.put(3, 4); //1.0
		freq.put("[3, 4]", 1.0);
		relations.put(4, 5); //0.125
		freq.put("[4, 5]", 0.125);
		relations.put(4, 17); //0.125
		freq.put("[4, 17]", 0.25);
		relations.put(4, 27); //0.125
		freq.put("[4, 27]", 0.375);
		relations.put(4, 6); //0.125
		freq.put("[4, 6]", 0.5);
		relations.put(4, 2); //0.5
		freq.put("[4, 2]", 1.0);
		
		// interleaving concurrency
		relations.put(5, 7); //1.0
		freq.put("[5, 7]", 1.0);
		relations.put(6, 7); //1.0
		freq.put("[6, 7]", 1.0);
		relations.put(17, 15); //1.0
		freq.put("[17, 15]", 1.0);
		relations.put(27, 16); //1.0
		freq.put("[27, 16]", 1.0);
		relations.put(15, 8); //1.0
		freq.put("[15, 8]", 1.0);
		relations.put(16, 8); //1.0
		freq.put("[16, 8]", 1.0);
		relations.put(7, 8); //1.0
		freq.put("[7, 8]", 1.0);
		
		// continuing regular flow of the process
		relations.put(8, 9); //0.5
		freq.put("[8, 9]", 0.5);
		relations.put(8, 10); //0.5
		freq.put("[8, 10]", 1.0);
		relations.put(10, 11); //1.0
		freq.put("[10, 11]", 1.0);
		relations.put(11, 12); //0.5
		freq.put("[11, 12]", 0.5);
		relations.put(11, 13); //0.5
		freq.put("[11, 13]", 1.0);
		relations.put(12, 13); //1.0
		freq.put("[12, 13]", 1.0);
		relations.put(13, 14); //0.5
		freq.put("[13, 14]", 0.5);
		relations.put(13, 18); //0.5
		freq.put("[13, 18]", 1.0);
	} //O2
	
	public void createGeneratedO3() {
		clearAll();
		
		activities.put(1, "Check application form completeness");
		activities.put(2, "Return application to applicant");
		activities.put(3, "Receive updated application");
		activities.put(4, "Check revised form");
		activities.put(5, "Check credit history");
		activities.put(15, "Check credit history");
		activities.put(6, "Assess loan risk");
		activities.put(7, "Appraise property");
		activities.put(17, "Appraise property");
		activities.put(8, "Assess eligibility");
		activities.put(9, "Reject application");
		activities.put(10, "Prepare acceptance pack");
		activities.put(11, "Check if home insurance quote is requested");
		activities.put(12, "Send home insurance quote");
		activities.put(13, "Verify repayment agreement");
		activities.put(14, "Cancel application");
		activities.put(18, "Approve application");
		
		relations.put(1, 5); //0.5
		freq.put("[1, 5]", 0.25);
		relations.put(1, 17); //0.5
		freq.put("[1, 17]", 0.5);
		relations.put(1, 2); //0.5
		freq.put("[1, 2]", 1.0);
		relations.put(2, 3); //1.0
		freq.put("[2, 3]", 1.0);
		relations.put(3, 4); //1.0
		freq.put("[3, 4]", 1.0);
		relations.put(4, 5); //0.1667
		freq.put("[4, 5]", 0.1667);
		relations.put(4, 15); //0.1667
		freq.put("[4, 15]", 0.3333);
		relations.put(4, 27); //0.1667
		freq.put("[4, 27]", 0.5);
		relations.put(4, 2); //0.5
		freq.put("[4, 2]", 1.0);
		
		// interleaving concurrency
		relations.put(5, 7); //1.0
		freq.put("[5, 7]", 1.0);
		relations.put(7, 6); //1.0
		freq.put("[7, 6]", 1.0);
		relations.put(17, 15); //1.0
		freq.put("[17, 15]", 1.0);
		relations.put(15, 6); //1.0
		freq.put("[15, 6]", 1.0);
		relations.put(6, 8); //1.0
		freq.put("[6, 8]", 1.0);
		
		// continuing regular flow of the process
		relations.put(8, 9); //0.5
		freq.put("[8, 9]", 0.5);
		relations.put(8, 10); //0.5
		freq.put("[8, 10]", 1.0);
		relations.put(10, 11); //1.0
		freq.put("[10, 11]", 1.0);
		relations.put(11, 12); //0.5
		freq.put("[11, 12]", 0.5);
		relations.put(11, 13); //0.5
		freq.put("[11, 13]", 1.0);
		relations.put(12, 13); //1.0
		freq.put("[12, 13]", 1.0);
		relations.put(13, 14); //0.5
		freq.put("[13, 14]", 0.5);
		relations.put(13, 18); //0.5
		freq.put("[13, 18]", 1.0);
	} //O3
}
